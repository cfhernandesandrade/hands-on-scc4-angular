import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DestinatariosComponent } from './destinatarios/destinatarios.component';
import { RemetentesComponent } from './remetentes/remetentes.component';
import { AppComponent } from './app.component';
import { InicioComponent } from './inicio/inicio.component';
import { CadastrarComponent } from './remetentes/cadastrar/cadastrar.component';
import { EditarComponent } from './remetentes/editar/editar.component';
import { MensagemComponent } from './destinatarios/mensagem/mensagem.component';


const routes: Routes = [
  {
    path: '',component:InicioComponent
  },
  {
    path: 'destinatarios',component:DestinatariosComponent
  },
  {
    path: 'remetentes',component:RemetentesComponent
  },
  {
    path: 'remetentes/cadastrar', component: CadastrarComponent
  },
  {
    path: 'remetentes/editar/:id', component: EditarComponent
  },
  {
    path: 'destinatarios/mensagem', component: MensagemComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
