import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({providedIn: 'root'})
export class ApiService {
  [x: string]: any;

  baseUrl = 'http://localhost:9088/remetentes';
  baseUrlDestinatarios = 'http://localhost:9088/destinatarios';

  constructor(private http: HttpClient) { }

  uploadCsv(file: any) {
    return this.http.post<any>(this.baseUrlDestinatarios + '/upload-csv-destinatarios', file);
  }

  listarUnico(id: any) {
    return this.http.get<Object>(this.baseUrl + '/remetente/'+ id);
  }

  listar() {
    return this.http.get<Array<Object[]>>(this.baseUrl + '/listarRemetentes');
  }

  listarDestinatarios() {
    return this.http.get<Array<Object[]>>(this.baseUrlDestinatarios + '/listarDestinatarios');
  }

  enviar(formulario: any) {
    return this.http.post(this.baseUrlDestinatarios + '/recebeMensagem', formulario);
  }

  editar(remetente: any, id: any ) {
    return this.http.put(this.baseUrl + '/editar/'+ id, remetente);
  }

  criar(remetente: any) {
    return this.http.post(this.baseUrl + '/cadastrarRemetente', remetente);
  }

  deletar(id: any) {
    return this.http.delete(this.baseUrl + '/deletar/' + id);
  }

}
