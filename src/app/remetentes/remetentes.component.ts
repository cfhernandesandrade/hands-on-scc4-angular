import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { faCoffee, faTrashAlt, faUserEdit } from '@fortawesome/free-solid-svg-icons';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-remetentes',
  templateUrl: './remetentes.component.html',
  styleUrls: ['./remetentes.component.css']
})
export class RemetentesComponent implements OnInit {

  faCoffee = faCoffee;
  faTrashAlt = faTrashAlt;
  faUserEdit = faUserEdit;

  //const localUrl = 'http://localhost:9088/';
  listaDeRementetes: Object[] = [];

  remetenteUnico: Object = [];

  constructor(private service: ApiService) { 
  }

  ngOnInit() {
    this.service.listar().subscribe(listaDeRementetes => {
      this.listaDeRementetes = listaDeRementetes
    });
  }

  deletar(id: any) {
    this.service.deletar(id).subscribe(resposta => {
      alert("Remetente deletado com sucesso!");
    });
  }

}
