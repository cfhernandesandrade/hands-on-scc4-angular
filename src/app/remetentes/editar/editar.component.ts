
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute, Params  } from "@angular/router";

import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})

export class EditarComponent implements OnInit {

  urlBuscar = 'http://localhost:9088/remetentes/remetente/{id}';
  urlEditar = 'http://localhost:9088/remetentes/editar/{id}';
  remetente: any;

  remetenteUnico: Object = [];
  actRoute: any;

  constructor(private service: ApiService, private route: ActivatedRoute) {
    
   }

  id: any;
  ngOnInit() {
    this.remetente = {};
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          //console.log(this.id);
        }
      );
    
    this.service.listarUnico(this.id).subscribe(remetente => {
      //console.log(remetente);
      this.remetenteUnico = remetente;
    });
    
  }

  editar(frm: FormGroup) {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          console.log(this.id);
        }
      );
    this.service.editar(this.remetenteUnico, this.id).subscribe(resposta => {
      console.log(this.id);
      frm.reset();
    })
  }

}
