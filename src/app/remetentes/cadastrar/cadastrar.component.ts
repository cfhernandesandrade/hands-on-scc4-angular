import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';

import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-cadastrar',
  templateUrl: './cadastrar.component.html',
  styleUrls: ['./cadastrar.component.css']
})
export class CadastrarComponent implements OnInit {

  url = `http://localhost:9088/remetentes/cadastrarRemetente`;

  remetente: any;

  constructor(private service: ApiService) {
    //http.post(url, )
  }

  ngOnInit() {
    this.remetente = {};
  }

  criar(frm: FormGroup) {
    this.service.criar(this.remetente).subscribe(resposta => {
      frm.reset();
    });
  }

}