import { Component, OnInit } from '@angular/core';
import { Params } from '@fortawesome/fontawesome-svg-core';
import { ApiService } from 'src/app/api.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, NgForm, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { map } from "rxjs/operators";

@Component({
  selector: 'app-mensagem',
  templateUrl: './mensagem.component.html',
  styleUrls: ['./mensagem.component.css']
})
export class MensagemComponent implements OnInit {

  formulario: FormGroup;

  remetente: any;
  remetenteUnico: Object = [];
  actRoute: any;
  url: 'http://localhost:9088/destinatarios/recebeMensagem';
  contactForm: FormGroup;
  route: any;

  
  constructor(private formBuilder: FormBuilder, private service: ApiService) {
    
  }

  listaDeRementetes: Object[] = [];
  listaDeDestinatarios: Object[] = [];

  id: any;
  ngOnInit() {
    this.formulario = this.formBuilder.group({
      nomeRemetente: [null],
      listaDestinatariosMensagem: [null],
      mensagemForm: [null]

    });

    this.remetente = {};
    this.service.listar().subscribe(listaDeRementetes => {
      this.listaDeRementetes = listaDeRementetes
    });

    this.service.listarDestinatarios().subscribe(listaDeDestinatarios => {
      this.listaDeDestinatarios = listaDeDestinatarios;
    });
    
  }

  listarUnico() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          //console.log(this.id);
        }
      );
    
    this.service.listarUnico(this.id).subscribe(remetente => {
      //console.log(remetente);
      this.remetenteUnico = remetente;
    });
  }

  enviar(formulario: FormGroup) {
    this.service.enviar(this.formulario.value).pipe(map(res => res)).subscribe(resposta => {
      this.formulario.reset();
    });
  }

}
