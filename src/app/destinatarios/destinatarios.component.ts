
import { ApiService } from '../api.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-destinatarios',
  templateUrl: './destinatarios.component.html',
  styleUrls: ['./destinatarios.component.css']
})
export class DestinatariosComponent implements OnInit {

  //constructor(private service: ApiService) { }

  baseUrlDestinatarios = 'http://localhost:9088/destinatarios';

  form: FormGroup;

  listaDeDestinatarios: Object[] = [];

  constructor(
    public fb: FormBuilder, 
    private http: HttpClient, 
    private service: ApiService) {
      this.form = this.fb.group({
        name: [''],
        avatar: [null]
      });
  }

  fileToUpload: File = null;

  ngOnInit() {

    this.service.listarDestinatarios().subscribe(listaDeDestinatarios => {
      this.listaDeDestinatarios = listaDeDestinatarios;
    });

  }

  /*
  uploadCsv(file: File) {
    console.log();
    this.service.uploadCsv(file).subscribe(
      (res) => console.log(file.type),
      (err) => console.log(file.type)
    );
  }
  */

  uploadFile(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.form.patchValue({
      avatar: file
    });
    this.form.get('avatar').updateValueAndValidity()
  }

  submitForm() {
    var formData: any = new FormData();
    formData.append("avatar", this.form.get('avatar').value);
  
    this.http.post(this.baseUrlDestinatarios + '/upload-csv-destinatarios', formData).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )
  }
  

}
