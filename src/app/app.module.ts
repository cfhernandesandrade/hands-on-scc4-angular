import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinatariosComponent } from './destinatarios/destinatarios.component';
import { RemetentesComponent } from './remetentes/remetentes.component';
import { InicioComponent } from './inicio/inicio.component';
import { CadastrarComponent } from './remetentes/cadastrar/cadastrar.component';
import { EditarComponent } from './remetentes/editar/editar.component';
import { MensagemComponent } from './destinatarios/mensagem/mensagem.component';
 
@NgModule({
  declarations: [
    AppComponent,
    DestinatariosComponent,
    RemetentesComponent,
    InicioComponent,
    CadastrarComponent,
    EditarComponent,
    MensagemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
